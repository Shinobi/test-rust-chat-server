# Rust Chat Server

A few experiments and tests using tokio and creating a pretty simple chat server.

## Crates used

- tokio : async handling crate
- reqwest : http crate working with tokio
