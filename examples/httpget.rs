/* 
    Simple example of a HTTP GET request
    using the reqwest crate.
*/

use std::error::Error;
use reqwest;

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let url = "https://www.rust-lang.org";
    let body = reqwest::get(url)
    .await?
    .text()
    .await?;
    print!("{:?}", body);

    Ok(())
}
