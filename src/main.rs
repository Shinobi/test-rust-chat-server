use tokio::{
    net::TcpListener,
    io::{AsyncBufReadExt, AsyncWriteExt, BufReader},
    sync::broadcast
};

#[tokio::main]
async fn main() {
    // listen to TCP connections attempts on <address>:<port>
    let listener: TcpListener = TcpListener::bind("localhost:8080").await.unwrap();

    // set a messaging bus between tasks
    let (tx,_rx) = broadcast::channel(10);

    loop { // for each client
        // accept the connection
        let (mut socket, addr) = listener.accept().await.unwrap();
        // clone the bus transmiter
        let tx = tx.clone();
        // subscribe as a listener to the bus
        let mut rx = tx.subscribe();

        tokio::spawn(async move {
            // split the socket in read and write
            let (reader, mut writer) = socket.split();

            // buffer wraper from tokio
            let mut reader = BufReader::new(reader);
            // String to store the message
            let mut line = String::new();

            // loop, reading messages from client
            // and sending him back new incoming messages
            loop {
                tokio::select! { // lets do concurrent reading and writing
                    // reading
                    result = reader.read_line(&mut line) => {
                        if result.unwrap() == 0 { // connection lost
                            break;
                        }
                        // new message on the bus! <message and sender addr>
                        tx.send((line.clone(), addr)).unwrap();
                        // clear that line!
                        line.clear();
                    }
                    // writing
                    result = rx.recv() => { // any message to send back ?
                        let (msg, other_addr) = result.unwrap();
                        // not from me? I'll send it :)
                        if addr != other_addr {
                            writer.write_all(msg.as_bytes()).await.unwrap();
                        }
                    }
                }
            }
        });
    }
}
